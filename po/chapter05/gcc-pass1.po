# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2024-09-07 06:38+0000\n"
"PO-Revision-Date: 2024-08-10 04:18+0000\n"
"Last-Translator: Jamenson Ferreira Espindula de Almeida Melo <jafesp@gmail.com>\n"
"Language-Team: Portuguese (Brazil) <https://translate.linuxfromscratch.org/projects/linux-from-scratch/chapter05_gcc-pass1/pt_BR/>\n"
"Language: pt_BR\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Generator: Weblate 5.6.2\n"

#. type: Attribute 'xreflabel' of: <sect1>
#: lfs-en/chapter05/gcc-pass1.xml:8
#, xml-text
msgid "gcc-pass1"
msgstr "gcc-pass1"

#. type: Content of: <sect1><sect1info><address>
#: lfs-en/chapter05/gcc-pass1.xml:14
#, no-wrap, xml-text
msgid "&gcc-url;"
msgstr "&gcc-url;"

#. type: Content of: <sect1><sect1info>
#: lfs-en/chapter05/gcc-pass1.xml:12
#, xml-text
msgid ""
"<productname>gcc-pass1</productname> <productnumber>&gcc-"
"version;</productnumber> <placeholder type=\"address\" id=\"0\"/>"
msgstr ""
"<productname>gcc-pass1</productname> <productnumber>&gcc-"
"version;</productnumber> <placeholder type=\"address\" id=\"0\"/>"

#. type: Content of: <sect1><title>
#: lfs-en/chapter05/gcc-pass1.xml:17
#, xml-text
msgid "GCC-&gcc-version; - Pass 1"
msgstr "GCC-&gcc-version; - Passagem 1"

#. type: Content of: <sect1><indexterm><primary>
#: lfs-en/chapter05/gcc-pass1.xml:20
#, xml-text
msgid "GCC"
msgstr "GCC"

#. type: Content of: <sect1><indexterm><secondary>
#: lfs-en/chapter05/gcc-pass1.xml:21
#, xml-text
msgid "tools, pass 1"
msgstr "ferramentas, passagem 1"

#. type: Content of: <sect1><sect2><segmentedlist><segtitle>
#: lfs-en/chapter05/gcc-pass1.xml:32
#, xml-text
msgid "&buildtime;"
msgstr "&buildtime;"

#. type: Content of: <sect1><sect2><segmentedlist><segtitle>
#: lfs-en/chapter05/gcc-pass1.xml:33
#, xml-text
msgid "&diskspace;"
msgstr "&diskspace;"

#. type: Content of: <sect1><sect2><segmentedlist><seglistitem><seg>
#: lfs-en/chapter05/gcc-pass1.xml:36
#, xml-text
msgid "&gcc-tmpp1-sbu;"
msgstr "&gcc-tmpp1-sbu;"

#. type: Content of: <sect1><sect2><segmentedlist><seglistitem><seg>
#: lfs-en/chapter05/gcc-pass1.xml:37
#, xml-text
msgid "&gcc-tmpp1-du;"
msgstr "&gcc-tmpp1-du;"

#. type: Content of: <sect1><sect2><title>
#: lfs-en/chapter05/gcc-pass1.xml:44
#, xml-text
msgid "Installation of Cross GCC"
msgstr "Instalação do GCC Cruzado"

#. type: Content of: <sect1><sect2><para>
#: lfs-en/chapter05/gcc-pass1.xml:46
#, xml-text
msgid ""
"GCC requires the GMP, MPFR and MPC packages. As these packages may not be "
"included in your host distribution, they will be built with GCC.  Unpack "
"each package into the GCC source directory and rename the resulting "
"directories so the GCC build procedures will automatically use them:"
msgstr ""
"O GCC exige os pacotes GMP, MPFR e MPC. Uma vez que esses pacotes talvez não"
" estejam incluídos na sua distribuição anfitriã, eles serão construídos com "
"o GCC. Desempacote cada pacote dentro do diretório de fonte do GCC e "
"renomeie os diretórios resultantes, de forma que os procedimentos de "
"construção do GCC automaticamente os usarão:"

#. type: Content of: <sect1><sect2><note><para>
#: lfs-en/chapter05/gcc-pass1.xml:52
#, xml-text
msgid ""
"There are frequent misunderstandings about this chapter.  The procedures are"
" the same as every other chapter, as explained earlier (<xref "
"linkend='buildinstr'/>).  First, extract the gcc-&gcc-version; tarball from "
"the sources directory, and then change to the directory created.  Only then "
"should you proceed with the instructions below."
msgstr ""
"Existem mal-entendidos frequentes a respeito deste capítulo. Os "
"procedimentos são os mesmos que todos os outros capítulos, conforme "
"explicados anteriormente (<xref linkend='buildinstr'/>). Primeiro, extraia o"
" tarball gcc-&gcc-version; a partir do diretório dos fontes e então mude "
"para o diretório criado. Somente então deveria você prosseguir com as "
"instruções abaixo."

#. type: Content of: <sect1><sect2><screen>
#: lfs-en/chapter05/gcc-pass1.xml:58
#, no-wrap, xml-text
msgid ""
"<userinput remap=\"pre\">tar -xf ../mpfr-&mpfr-version;.tar.xz\n"
"mv -v mpfr-&mpfr-version; mpfr\n"
"tar -xf ../gmp-&gmp-version;.tar.xz\n"
"mv -v gmp-&gmp-version; gmp\n"
"tar -xf ../mpc-&mpc-version;.tar.gz\n"
"mv -v mpc-&mpc-version; mpc</userinput>"
msgstr ""
"<userinput remap=\"pre\">tar -xf ../mpfr-&mpfr-version;.tar.xz\n"
"mv -v mpfr-&mpfr-version; mpfr\n"
"tar -xf ../gmp-&gmp-version;.tar.xz\n"
"mv -v gmp-&gmp-version; gmp\n"
"tar -xf ../mpc-&mpc-version;.tar.gz\n"
"mv -v mpc-&mpc-version; mpc</userinput>"

#. type: Content of: <sect1><sect2><para>
#: lfs-en/chapter05/gcc-pass1.xml:65
#, xml-text
msgid ""
"On x86_64 hosts, set the default directory name for 64-bit libraries to "
"<quote>lib</quote>:"
msgstr ""
"Em anfitriões x86_64, configure o nome padrão de diretório para bibliotecas "
"de 64 bits para <quote>lib</quote>:"

#. type: Content of: <sect1><sect2><screen>
#: lfs-en/chapter05/gcc-pass1.xml:68
#, no-wrap, xml-text
msgid ""
"<userinput remap=\"pre\">case $(uname -m) in\n"
"  x86_64)\n"
"    sed -e '/m64=/s/lib64/lib/' \\\n"
"        -i.orig gcc/config/i386/t-linux64\n"
" ;;\n"
"esac</userinput>"
msgstr ""
"<userinput remap=\"pre\">case $(uname -m) in\n"
"  x86_64)\n"
"    sed -e '/m64=/s/lib64/lib/' \\\n"
"        -i.orig gcc/config/i386/t-linux64\n"
" ;;\n"
"esac</userinput>"

#. type: Content of: <sect1><sect2><para>
#: lfs-en/chapter05/gcc-pass1.xml:75
#, xml-text
msgid ""
"The GCC documentation recommends building GCC in a dedicated build "
"directory:"
msgstr ""
"A documentação do GCC recomenda construir o GCC em um diretório de "
"construção dedicado:"

#. type: Content of: <sect1><sect2><screen>
#: lfs-en/chapter05/gcc-pass1.xml:78
#, no-wrap, xml-text
msgid ""
"<userinput remap=\"pre\">mkdir -v build\n"
"cd       build</userinput>"
msgstr ""
"<userinput remap=\"pre\">mkdir -v build\n"
"cd      build</userinput>"

#. type: Content of: <sect1><sect2><para>
#: lfs-en/chapter05/gcc-pass1.xml:81
#, xml-text
msgid "Prepare GCC for compilation:"
msgstr "Prepare o GCC para compilação:"

#. type: Content of: <sect1><sect2><screen>
#: lfs-en/chapter05/gcc-pass1.xml:83
#, no-wrap, xml-text
msgid ""
"<userinput remap=\"configure\">../configure                  \\\n"
"    --target=$LFS_TGT         \\\n"
"    --prefix=$LFS/tools       \\\n"
"    --with-glibc-version=&glibc-version; \\\n"
"    --with-sysroot=$LFS       \\\n"
"    --with-newlib             \\\n"
"    --without-headers         \\\n"
"    --enable-default-pie      \\\n"
"    --enable-default-ssp      \\\n"
"    --disable-nls             \\\n"
"    --disable-shared          \\\n"
"    --disable-multilib        \\\n"
"    --disable-threads         \\\n"
"    --disable-libatomic       \\\n"
"    --disable-libgomp         \\\n"
"    --disable-libquadmath     \\\n"
"    --disable-libssp          \\\n"
"    --disable-libvtv          \\\n"
"    --disable-libstdcxx       \\\n"
"    --enable-languages=c,c++</userinput>"
msgstr ""
"<userinput remap=\"configure\">../configure                  \\\n"
"    --target=$LFS_TGT         \\\n"
"    --prefix=$LFS/tools       \\\n"
"    --with-glibc-version=&glibc-version; \\\n"
"    --with-sysroot=$LFS       \\\n"
"    --with-newlib             \\\n"
"    --without-headers         \\\n"
"    --enable-default-pie      \\\n"
"    --enable-default-ssp      \\\n"
"    --disable-nls             \\\n"
"    --disable-shared          \\\n"
"    --disable-multilib        \\\n"
"    --disable-threads         \\\n"
"    --disable-libatomic       \\\n"
"    --disable-libgomp         \\\n"
"    --disable-libquadmath     \\\n"
"    --disable-libssp          \\\n"
"    --disable-libvtv          \\\n"
"    --disable-libstdcxx       \\\n"
"    --enable-languages=c,c++</userinput>"

#. type: Content of: <sect1><sect2><variablelist><title>
#: lfs-en/chapter05/gcc-pass1.xml:104
#, xml-text
msgid "The meaning of the configure options:"
msgstr "O significado das opções do configure:"

#. type: Content of: <sect1><sect2><variablelist><varlistentry><term>
#: lfs-en/chapter05/gcc-pass1.xml:107
#, xml-text
msgid "<parameter>--with-glibc-version=&glibc-version;</parameter>"
msgstr "<parameter>--with-glibc-version=&glibc-version;</parameter>"

#. type: Content of:
#. <sect1><sect2><variablelist><varlistentry><listitem><para>
#: lfs-en/chapter05/gcc-pass1.xml:109
#, xml-text
msgid ""
"This option specifies the version of Glibc which will be used on the target."
" It is not relevant to the libc of the host distro because everything "
"compiled by pass1 GCC will run in the chroot environment, which is isolated "
"from libc of the host distro."
msgstr ""
"Essa opção especifica a versão da Glibc que será usada no alvo. Ela não é "
"relevante para a libc da distribuição anfitriã, pois tudo compilado pela "
"passagem 1 do GCC executará no ambiente chroot, o qual é isolado da libc da "
"distribuição anfitriã."

#. type: Content of: <sect1><sect2><variablelist><varlistentry><term>
#: lfs-en/chapter05/gcc-pass1.xml:118
#, xml-text
msgid "<parameter>--with-newlib</parameter>"
msgstr "<parameter>--with-newlib</parameter>"

#. type: Content of:
#. <sect1><sect2><variablelist><varlistentry><listitem><para>
#: lfs-en/chapter05/gcc-pass1.xml:120
#, xml-text
msgid ""
"Since a working C library is not yet available, this ensures that the "
"inhibit_libc constant is defined when building libgcc. This prevents the "
"compiling of any code that requires libc support."
msgstr ""
"Uma vez que uma biblioteca C funcional ainda não está disponível, isso "
"assegura que a constante inhibit_libc esteja definida quando da construção "
"de libgcc. Isso evita a compilação de qualquer código que exija suporte à "
"libc."

#. type: Content of: <sect1><sect2><variablelist><varlistentry><term>
#: lfs-en/chapter05/gcc-pass1.xml:127
#, xml-text
msgid "<parameter>--without-headers</parameter>"
msgstr "<parameter>--without-headers</parameter>"

#. type: Content of:
#. <sect1><sect2><variablelist><varlistentry><listitem><para>
#: lfs-en/chapter05/gcc-pass1.xml:129
#, xml-text
msgid ""
"When creating a complete cross-compiler, GCC requires standard headers "
"compatible with the target system. For our purposes these headers will not "
"be needed. This switch prevents GCC from looking for them."
msgstr ""
"Quando da criação de um compilador cruzado completo, o GCC exige cabeçalhos "
"padrão compatíveis com o sistema alvo. Para nossos propósitos esses "
"cabeçalhos não serão necessários. Essa chave evita que o GCC procure por "
"eles."

#. type: Content of: <sect1><sect2><variablelist><varlistentry><term>
#: lfs-en/chapter05/gcc-pass1.xml:137
#, xml-text
msgid "<parameter>--enable-default-pie and --enable-default-ssp</parameter>"
msgstr "<parameter>--enable-default-pie e --enable-default-ssp</parameter>"

#. type: Content of:
#. <sect1><sect2><variablelist><varlistentry><listitem><para>
#: lfs-en/chapter05/gcc-pass1.xml:140
#, xml-text
msgid ""
"Those switches allow GCC to compile programs with some hardening security "
"features (more information on those in the <xref linkend=\"pie-ssp-info\"/> "
"in chapter 8) by default. They are not strictly needed at this stage, since "
"the compiler will only produce temporary executables. But it is cleaner to "
"have the temporary packages be as close as possible to the final ones."
msgstr ""
"Essas chaves permitem que o GCC compile aplicativos com alguns recursos de "
"segurança reforçados (mais informação a respeito delas na <xref "
"linkend=\"pie-ssp-info\"/> no capítulo 8) por padrão. Elas não são "
"estritamente necessárias neste estágio, pois o compilador produzirá apenas "
"executáveis temporários. Mas, é mais limpo ter os pacotes temporários o mais"
" próximo possível dos finais."

#. type: Content of: <sect1><sect2><variablelist><varlistentry><term>
#: lfs-en/chapter05/gcc-pass1.xml:151
#, xml-text
msgid "<parameter>--disable-shared</parameter>"
msgstr "<parameter>--disable-shared</parameter>"

#. type: Content of:
#. <sect1><sect2><variablelist><varlistentry><listitem><para>
#: lfs-en/chapter05/gcc-pass1.xml:153
#, xml-text
msgid ""
"This switch forces GCC to link its internal libraries statically. We need "
"this because the shared libraries require Glibc, which is not yet installed "
"on the target system."
msgstr ""
"Essa chave força o GCC a vincular as bibliotecas internas dele "
"estaticamente. Nós precisamos disso, pois as bibliotecas compartilhadas "
"exigem a Glibc, que ainda não está instalada no sistema alvo."

#. type: Content of: <sect1><sect2><variablelist><varlistentry><term>
#: lfs-en/chapter05/gcc-pass1.xml:160
#, xml-text
msgid "<parameter>--disable-multilib</parameter>"
msgstr "<parameter>--disable-multilib</parameter>"

#. type: Content of:
#. <sect1><sect2><variablelist><varlistentry><listitem><para>
#: lfs-en/chapter05/gcc-pass1.xml:162
#, xml-text
msgid ""
"On x86_64, LFS does not support a multilib configuration.  This switch is "
"harmless for x86."
msgstr ""
"Em x86_64, o LFS não suporta uma configuração multi bibliotecas. Essa chave "
"é inofensiva para x86."

#. type: Content of: <sect1><sect2><variablelist><varlistentry><term>
#: lfs-en/chapter05/gcc-pass1.xml:168
#, xml-text
msgid ""
"<parameter>--disable-threads, --disable-libatomic, --disable-libgomp, "
"--disable-libquadmath, --disable-libssp, --disable-libvtv, --disable-"
"libstdcxx</parameter>"
msgstr ""
"<parameter>--disable-threads, --disable-libatomic, --disable-libgomp, "
"--disable-libquadmath, --disable-libssp, --disable-libvtv e --disable-"
"libstdcxx</parameter>"

#. type: Content of:
#. <sect1><sect2><variablelist><varlistentry><listitem><para>
#: lfs-en/chapter05/gcc-pass1.xml:173
#, xml-text
msgid ""
"These switches disable support for threading, libatomic, libgomp, "
"libquadmath, libssp, libvtv, and the C++ standard library respectively. "
"These features may fail to compile when building a cross-compiler and are "
"not necessary for the task of cross-compiling the temporary libc."
msgstr ""
"Essas chaves desabilitam o suporte para threading, libatomic, libgomp, "
"libquadmath, libssp, libvtv e à biblioteca padrão C++ respectivamente. Esses"
" recursos possivelmente falhem para compilar quando da construção de um "
"compilador cruzado e não são necessários para a tarefa de compilar "
"cruzadamente a libc temporária."

#. type: Content of: <sect1><sect2><variablelist><varlistentry><term>
#: lfs-en/chapter05/gcc-pass1.xml:182
#, xml-text
msgid "<parameter>--enable-languages=c,c++</parameter>"
msgstr "<parameter>--enable-languages=c,c++</parameter>"

#. type: Content of:
#. <sect1><sect2><variablelist><varlistentry><listitem><para>
#: lfs-en/chapter05/gcc-pass1.xml:184
#, xml-text
msgid ""
"This option ensures that only the C and C++ compilers are built.  These are "
"the only languages needed now."
msgstr ""
"Essa opção garante que apenas os compiladores C e C++ sejam construídos. "
"Essas são as únicas linguagens necessárias agora."

#. type: Content of: <sect1><sect2><para>
#: lfs-en/chapter05/gcc-pass1.xml:191
#, xml-text
msgid "Compile GCC by running:"
msgstr "Compile o GCC executando:"

#. type: Content of: <sect1><sect2><screen>
#: lfs-en/chapter05/gcc-pass1.xml:193
#, no-wrap, xml-text
msgid "<userinput remap=\"make\">make</userinput>"
msgstr "<userinput remap=\"make\">make</userinput>"

#. type: Content of: <sect1><sect2><para>
#: lfs-en/chapter05/gcc-pass1.xml:195
#, xml-text
msgid "Install the package:"
msgstr "Instale o pacote:"

#. type: Content of: <sect1><sect2><screen>
#: lfs-en/chapter05/gcc-pass1.xml:197
#, no-wrap, xml-text
msgid "<userinput remap=\"install\">make install</userinput>"
msgstr "<userinput remap=\"install\">make install</userinput>"

#. type: Content of: <sect1><sect2><para>
#: lfs-en/chapter05/gcc-pass1.xml:199
#, xml-text
msgid ""
"This build of GCC has installed a couple of internal system headers.  "
"Normally one of them, <filename>limits.h</filename>, would in turn include "
"the corresponding system <filename>limits.h</filename> header, in this case,"
" <filename>$LFS/usr/include/limits.h</filename>. However, at the time of "
"this build of GCC <filename>$LFS/usr/include/limits.h</filename> does not "
"exist, so the internal header that has just been installed is a partial, "
"self-contained file and does not include the extended features of the system"
" header. This is adequate for building Glibc, but the full internal header "
"will be needed later.  Create a full version of the internal header using a "
"command that is identical to what the GCC build system does in normal "
"circumstances:"
msgstr ""
"Essa construção do GCC instalou um par de cabeçalhos internos de sistema. "
"Normalmente um deles, <filename>limits.h</filename>, sequencialmente "
"incluiria o correspondente cabeçalho de sistema "
"<filename>limits.h</filename>, nesse caso, "
"<filename>$LFS/usr/include/limits.h</filename>. Entretanto, ao tempo dessa "
"construção do GCC, <filename>$LFS/usr/include/limits.h</filename> não "
"existe, de forma que o cabeçalho interno que tenha sido instalado é um "
"arquivo parcial, auto-contido, e não inclui os recursos estendidos do "
"cabeçalho de sistema. Isso é adequado para a construção da Glibc, porém o "
"cabeçalho interno completo será necessário posteriormente. Crie uma versão "
"completa do cabeçalho interno usando um comando que é idêntico ao que o "
"sistema de construção do GCC faz em circunstâncias normais:"

#. type: Content of: <sect1><sect2><note><para>
#: lfs-en/chapter05/gcc-pass1.xml:212
#, xml-text
msgid ""
"The command below shows an example of nested command substitution using two "
"methods: backquotes and a <literal>$()</literal> construct.  It could be "
"rewritten using the same method for both substitutions, but is shown this "
"way to demonstrate how they can be mixed.  Generally the "
"<literal>$()</literal> method is preferred."
msgstr ""
"O comando abaixo mostra um exemplo da substituição de comando aninhada "
"usando dois métodos: aspas invertidas e uma construção "
"<literal>$()</literal>. Poderia ser reescrito usando o mesmo método para "
"ambas as substituições, porém é mostrado dessa maneira para demonstrar o "
"como eles podem ser misturados. Geralmente o método <literal>$()</literal> é"
" preferido."

#. type: Content of: <sect1><sect2><screen>
#: lfs-en/chapter05/gcc-pass1.xml:219
#, no-wrap, xml-text
msgid ""
"<userinput remap=\"install\">cd ..\n"
"cat gcc/limitx.h gcc/glimits.h gcc/limity.h > \\\n"
"  `dirname $($LFS_TGT-gcc -print-libgcc-file-name)`/include/limits.h</userinput>"
msgstr ""
"<userinput remap=\"install\">cd ..\n"
"cat gcc/limitx.h gcc/glimits.h gcc/limity.h > \\\n"
"  `dirname $($LFS_TGT-gcc -print-libgcc-file-name)`/include/limits.h</userinput>"

#. type: Content of: <sect1><sect2><para>
#: lfs-en/chapter05/gcc-pass1.xml:227
#, xml-text
msgid ""
"Details on this package are located in <xref linkend=\"contents-gcc\" "
"role=\".\"/>"
msgstr ""
"Detalhes acerca deste pacote estão localizados na <xref linkend=\"contents-"
"gcc\" role=\".\"/>"
