# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2023-03-11 18:54+0000\n"
"PO-Revision-Date: 2023-03-28 00:47+0000\n"
"Last-Translator: Jamenson Ferreira Espindula de Almeida Melo <jafesp@gmail."
"com>\n"
"Language-Team: Portuguese (Brazil) <https://translate.linuxfromscratch.org/"
"projects/linux-from-scratch-11-3/chapter08_pkgconfig/pt_BR/>\n"
"Language: pt_BR\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Generator: Weblate 4.16.2\n"

#. type: Content of: <sect1><sect1info><address>
#: lfs-en/chapter08/pkgconfig.xml:14
#, no-wrap
msgid "&pkgconfig-url;"
msgstr "&pkgconfig-url;"

#. type: Content of: <sect1><sect1info>
#: lfs-en/chapter08/pkgconfig.xml:12
msgid ""
"<productname>pkg-config</productname> "
"<productnumber>&pkgconfig-version;</productnumber> <placeholder "
"type=\"address\" id=\"0\"/>"
msgstr ""
"<productname>pkg-config</productname> <productnumber>&pkgconfig-version;</"
"productnumber> <placeholder type=\"address\" id=\"0\"/>"

#. type: Content of: <sect1><title>
#: lfs-en/chapter08/pkgconfig.xml:17
msgid "Pkg-config-&pkgconfig-version;"
msgstr "Pkg-config-&pkgconfig-version;"

#. type: Content of: <sect1><indexterm><primary>
#: lfs-en/chapter08/pkgconfig.xml:20
msgid "Pkgconfig"
msgstr "Pkgconfig"

#. type: Content of: <sect1><sect2><para>
#: lfs-en/chapter08/pkgconfig.xml:26
msgid ""
"The pkg-config package contains a tool for passing the include path and/or "
"library paths to build tools during the configure and make phases of package "
"installations."
msgstr ""
"O pacote pkg-config contém uma ferramenta para passar o caminho include e "
"(ou) caminhos de biblioteca para ferramentas de construção durante as fases "
"configure e make de instalações de pacote."

#. type: Content of: <sect1><sect2><segmentedlist><segtitle>
#: lfs-en/chapter08/pkgconfig.xml:31
msgid "&buildtime;"
msgstr "&buildtime;"

#. type: Content of: <sect1><sect2><segmentedlist><segtitle>
#: lfs-en/chapter08/pkgconfig.xml:32
msgid "&diskspace;"
msgstr "&diskspace;"

#. type: Content of: <sect1><sect2><segmentedlist><seglistitem><seg>
#: lfs-en/chapter08/pkgconfig.xml:35
msgid "&pkgconfig-fin-sbu;"
msgstr "&pkgconfig-fin-sbu;"

#. type: Content of: <sect1><sect2><segmentedlist><seglistitem><seg>
#: lfs-en/chapter08/pkgconfig.xml:36
msgid "&pkgconfig-fin-du;"
msgstr "&pkgconfig-fin-du;"

#. type: Content of: <sect1><sect2><title>
#: lfs-en/chapter08/pkgconfig.xml:43
msgid "Installation of Pkg-config"
msgstr "Instalação do Pkg-config"

#. type: Content of: <sect1><sect2><para>
#: lfs-en/chapter08/pkgconfig.xml:45
msgid "Prepare Pkg-config for compilation:"
msgstr "Prepare Pkg-config para compilação:"

#. type: Content of: <sect1><sect2><screen>
#: lfs-en/chapter08/pkgconfig.xml:47
#, no-wrap
msgid ""
"<userinput remap=\"configure\">./configure --prefix=/usr              \\\n"
"            --with-internal-glib       \\\n"
"            --disable-host-tool        \\\n"
"            "
"--docdir=/usr/share/doc/pkg-config-&pkgconfig-version;</userinput>"
msgstr ""
"<userinput remap=\"configure\">./configure --prefix=/usr              \\\n"
"            --with-internal-glib       \\\n"
"            --disable-host-tool        \\\n"
"            --docdir=/usr/share/doc/pkg-config-&pkgconfig-"
"version;</userinput>"

#. type: Content of: <sect1><sect2><variablelist><title>
#: lfs-en/chapter08/pkgconfig.xml:53
msgid "The meaning of the new configure options:"
msgstr "O significado das novas opções do configure:"

#. type: Content of: <sect1><sect2><variablelist><varlistentry><term>
#: lfs-en/chapter08/pkgconfig.xml:56
msgid "<parameter>--with-internal-glib</parameter>"
msgstr "<parameter>--with-internal-glib</parameter>"

#. type: Content of: <sect1><sect2><variablelist><varlistentry><listitem><para>
#: lfs-en/chapter08/pkgconfig.xml:58
msgid ""
"This will allow pkg-config to use its internal version of Glib because an "
"external version is not available in LFS."
msgstr ""
"Isso permitirá que o pkg-config use a versão interna dele da Glib, pois uma "
"versão externa não está disponível no LFS."

#. type: Content of: <sect1><sect2><variablelist><varlistentry><term>
#: lfs-en/chapter08/pkgconfig.xml:64
msgid "<parameter>--disable-host-tool</parameter>"
msgstr "<parameter>--disable-host-tool</parameter>"

#. type: Content of: <sect1><sect2><variablelist><varlistentry><listitem><para>
#: lfs-en/chapter08/pkgconfig.xml:66
msgid ""
"This option disables the creation of an undesired hard link to the "
"pkg-config program."
msgstr ""
"Essa opção desabilita a criação de um indesejado link rígido para o "
"aplicativo pkg-config."

#. type: Content of: <sect1><sect2><para>
#: lfs-en/chapter08/pkgconfig.xml:73
msgid "Compile the package:"
msgstr "Compile o pacote:"

#. type: Content of: <sect1><sect2><screen>
#: lfs-en/chapter08/pkgconfig.xml:75
#, no-wrap
msgid "<userinput remap=\"make\">make</userinput>"
msgstr "<userinput remap=\"make\">make</userinput>"

#. type: Content of: <sect1><sect2><para>
#: lfs-en/chapter08/pkgconfig.xml:77
msgid "To test the results, issue:"
msgstr "Para testar os resultados, emita:"

#. type: Content of: <sect1><sect2><screen>
#: lfs-en/chapter08/pkgconfig.xml:79
#, no-wrap
msgid "<userinput remap=\"test\">make check</userinput>"
msgstr "<userinput remap=\"test\">make check</userinput>"

#. type: Content of: <sect1><sect2><para>
#: lfs-en/chapter08/pkgconfig.xml:81
msgid "Install the package:"
msgstr "Instale o pacote:"

#. type: Content of: <sect1><sect2><screen>
#: lfs-en/chapter08/pkgconfig.xml:83
#, no-wrap
msgid "<userinput remap=\"install\">make install</userinput>"
msgstr "<userinput remap=\"install\">make install</userinput>"

#. type: Content of: <sect1><sect2><title>
#: lfs-en/chapter08/pkgconfig.xml:88
msgid "Contents of Pkg-config"
msgstr "Conteúdo do Pkg-config"

#. type: Content of: <sect1><sect2><segmentedlist><segtitle>
#: lfs-en/chapter08/pkgconfig.xml:91
msgid "Installed program"
msgstr "Aplicativo instalado"

#. type: Content of: <sect1><sect2><segmentedlist><segtitle>
#: lfs-en/chapter08/pkgconfig.xml:92
msgid "Installed directory"
msgstr "Diretórios instalados"

#. type: Content of: <sect1><sect2><variablelist><varlistentry><listitem><indexterm><primary>
#: lfs-en/chapter08/pkgconfig.xml:95 lfs-en/chapter08/pkgconfig.xml:112
msgid "pkg-config"
msgstr "pkg-config"

#. type: Content of: <sect1><sect2><segmentedlist><seglistitem><seg>
#: lfs-en/chapter08/pkgconfig.xml:96
msgid "/usr/share/doc/pkg-config-&pkgconfig-version;"
msgstr "/usr/share/doc/pkg-config-&pkgconfig-version;"

#. type: Content of: <sect1><sect2><variablelist><bridgehead>
#: lfs-en/chapter08/pkgconfig.xml:101
msgid "Short Descriptions"
msgstr "Descrições Curtas"

#. type: Content of: <sect1><sect2><variablelist>
#: lfs-en/chapter08/pkgconfig.xml:102
msgid "<?dbfo list-presentation=\"list\"?> <?dbhtml list-presentation=\"table\"?>"
msgstr ""
"<?dbfo list-presentation=\"list\"?> <?dbhtml list-presentation=\"table\"?>"

#. type: Content of: <sect1><sect2><variablelist><varlistentry><term>
#: lfs-en/chapter08/pkgconfig.xml:107
msgid "<command>pkg-config </command>"
msgstr "<command>pkg-config </command>"

#. type: Content of: <sect1><sect2><variablelist><varlistentry><listitem><para>
#: lfs-en/chapter08/pkgconfig.xml:109
msgid "Returns meta information for the specified library or package"
msgstr "Retorna meta informação para a biblioteca ou pacote especificada"
